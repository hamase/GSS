module.exports = function(grunt) {
  grunt.initConfig({
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'assets/sass/',
          src: ['*.scss'],
          dest: 'assets/css/',
          ext: '.css'
        }]
      }
    },
    watch: {
      source: {
        files: ['assets/sass/*.scss'],
        tasks: ['sass'],
        options: {
          livereload: true
        }
      }
    },
    browserSync: {
      dev: {
        bsFiles: {
          src : [
          'assets/css/*.css',
          '*.html',
          'assets/sass/*.scss'
          ]
        },
        options: {
          watchTask: true,
          proxy : "localhost/GSS/"
        }
      }
    }
  });

  //grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');


  grunt.registerTask('default', ['watch', 'sass']);
};
